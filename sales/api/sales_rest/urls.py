from django.urls import path
from .views import (
    api_delete_autosale, api_list_customers, api_list_salespeople,
    api_show_salesperson, api_list_autosales
    )

urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("autosales/", api_list_autosales, name="api_list_autosales"),
    path("autosales/<int:pk>/", api_delete_autosale, name="api_delete_autosale"),
]
