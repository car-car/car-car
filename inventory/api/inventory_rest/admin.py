from django.contrib import admin
from .models import Manufacturer, VehicleModel, Automobile, AddOn


admin.site.register(Automobile)
admin.site.register(AddOn)
admin.site.register(Manufacturer)
admin.site.register(VehicleModel)
