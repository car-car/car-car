import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


class AddAutoSale extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            price: '',
            automobiles: [],
            salespeople: [],
            customers: [],
            formVisible: "shadow p-4 mt-4",
            successVisible: "d-none",
        };
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);

    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.sales_rep = data.salesperson;
        delete data.salesperson;
        delete data.automobiles;
        delete data.salespeople;
        delete data.customers;
        delete data.formVisible;
        delete data.successVisible;


        const autosaleUrl = `${process.env.REACT_APP_SALES_API}/api/autosales/`;
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(autosaleUrl, fetchConfig);
        if (response.ok) {

          const autoUrl = `${process.env.REACT_APP_INVENTORY_API}/api/automobiles/${this.state.automobile}/`
            const autoFetchConfig = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ sold: true })
        }
            const autoResponse = await fetch(autoUrl, autoFetchConfig)
            if (!autoResponse.ok) {
                console.error(autoResponse);
            }

          const cleared = {
            price: '',
            automobile: '',
            salesperson: '',
            customer: '',
            formVisible: "shadow p-4 mt-4 d-none",
            successVisible: "",
        }
        this.setState(cleared);
    }
    }

    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({price: value})
    }

    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({automobile: value})
    }

    handleSalespersonChange(event) {
        const value = event.target.value;
        this.setState({salesperson: value})
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({customer: value})
    }

    handleReset(event) {
        this.setState(
            {formVisible: "shadow p-4 mt-4",
            successVisible: "d-none"}
        );
    }

    async componentDidMount() {
        AllowedToVisit()
        const automobileUrl = `${process.env.REACT_APP_INVENTORY_API}/api/automobiles/`;
        const salespersonUrl = `${process.env.REACT_APP_SALES_API}/api/salespeople/`;
        const customerUrl = `${process.env.REACT_APP_SALES_API}/api/customers/`;

        const automobileResponse = await fetch(automobileUrl);
        const salespersonResponse = await fetch(salespersonUrl);
        const customerResponse = await fetch(customerUrl);

        if (automobileResponse.ok && salespersonResponse.ok && customerResponse.ok) {
          const automobileData = await automobileResponse.json();
          const salespersonData = await salespersonResponse.json();
          const customerData = await customerResponse.json();

          this.setState({

            customers: customerData.customers,
            automobiles: automobileData.autos,
            salespeople: salespersonData.salespeople
        });
        }
      }

    render() {
        return (
<div className="row">
        <div className="offset-3 col-6">
        <div className={this.state.successVisible}>
                        <div className="alert alert-success mt-4" role="alert">
                            Autosale added successfully!
                        </div>
                        <button className="btn btn-outline-success" onClick={this.handleReset}>
                            Add another!
                        </button>
                    </div>
            <div className={this.state.formVisible}>
            <h1>Create a new autosale</h1>
            <form onSubmit={this.handleSubmit} id="create-autosale-form">
              <div className="form-floating mb-3">
                <input onChange={this.handlePriceChange} value={this.state.price} placeholder="Price" max="10000000" min="1" required type="number" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleAutomobileChange} required id="automobile"
                        className="form-select" name="automobile" value={this.state.automobile}>
                    <option value="">Choose automobile</option>
                        {this.state.automobiles.map(automobile => {
                            if (automobile.sold === false) {
                            return (
                                <option key={automobile.vin} value={automobile.vin}>
                                    {automobile.vin}
                                </option>
                                );
                            } else {
                                return null
                            }
                        })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={this.handleSalespersonChange} required id="sales_rep"
                        className="form-select" name="sales_rep" value={this.state.salesperson}>
                    <option value="">Choose salesperson</option>
                        {this.state.salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.sales_person} value={salesperson.sales_person}>
                                    {salesperson.sales_person}
                                </option>
                                );
                        })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={this.handleCustomerChange} required id="customer"
                        className="form-select" name="customer" value={this.state.customer}>
                    <option value="">Choose customer</option>
                        {this.state.customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.name}>
                                    {customer.customer_name}
                                </option>
                                );
                        })}
                </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default AddAutoSale;
