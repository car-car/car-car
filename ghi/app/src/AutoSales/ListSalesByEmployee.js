import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


class ListSalesByEmployee extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        salespeople: [],
        autosales: [],
        salesPerson: '',
      };
      this.handleChange = this.handleChange.bind(this);
    }

    async handleChange(event) {
      const value = event.target.value;
      this.setState({ salesPerson: value });

      const autosaleUrl = `${process.env.REACT_APP_SALES_API}/api/autosales/`;
      const autosaleResponse = await fetch(autosaleUrl);

      if (autosaleResponse.ok) {
        const autosaleData = await autosaleResponse.json();

        if (this.state.salesPerson === "") {
          this.setState({ autosales: autosaleData.autosales });
        } else {
          let autosaleByEmployee = [];
          for (const autosale of autosaleData.autosales) {
            if (autosale.sales_rep.sales_person === this.state.salesPerson) {
                autosaleByEmployee.push(autosale);
            }
          }
          this.setState({ autosales: autosaleByEmployee });
        }
      }
    }

    async componentDidMount() {
      AllowedToVisit()
      const salespeopleUrl = `${process.env.REACT_APP_SALES_API}/api/salespeople/`;
      const autosaleUrl = `${process.env.REACT_APP_SALES_API}/api/autosales/`;
      const salespeopleResponse = await fetch(salespeopleUrl);
      const autosaleResponse = await fetch(autosaleUrl);

      if (salespeopleResponse.ok && autosaleResponse.ok) {
        const salespeopleData = await salespeopleResponse.json();
        const autosaleData = await autosaleResponse.json();

        this.setState({
            salespeople: salespeopleData.salespeople,
            autosales: autosaleData.autosales
         });
      }
    }

    render() {
      return (
        <div className="container">
            <p></p>
          <h2>Sales person history</h2>
          <div className="mb-3">
            <select onChange={this.handleChange} value={this.state.salesPerson} required name="salesPerson" id="salesPerson" className="form-select">
              <option value="">Choose Sales Person</option>
              {this.state.salespeople.map(salesperson => {
                return (
                  <option key={salesperson.sales_person} value={salesperson.sales_person}>
                    {salesperson.sales_person}
                  </option>
                )
              })}
            </select>
          </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Sales Person</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Sale Price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.autosales.map(autosale => {
                return (
                  <tr key={autosale.id}>
                    <td>{autosale.sales_rep.sales_person}</td>
                    <td>{autosale.customer.customer_name}</td>
                    <td>{autosale.automobile.vin}</td>
                    <td>$ {new Intl.NumberFormat().format(autosale.price)}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      );
    }
  }

export default ListSalesByEmployee;
