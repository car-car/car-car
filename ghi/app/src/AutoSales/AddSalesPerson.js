import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


class AddSalesPerson extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            salesPerson: '',
            employeeNum: 1000,
            formVisible: "shadow p-4 mt-4",
            successVisible: "d-none",
        };
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
        this.handleEmployeeNumChange = this.handleEmployeeNumChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.sales_person = data.salesPerson;
        delete data.salesPerson;
        data.employee_num = data.employeeNum;
        delete data.employeeNum;
        delete data.formVisible;
        delete data.successVisible;

        const customerUrl = `${process.env.REACT_APP_SALES_API}/api/salespeople/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                salesPerson: '',
                employeeNum: data.employee_num + 1,
                formVisible: "shadow p-4 mt-4 d-none",
                successVisible: "",
            }
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        AllowedToVisit()
        const salespeopleUrl = `${process.env.REACT_APP_SALES_API}/api/salespeople/`;
        const salespeopleResponse = await fetch(salespeopleUrl);

        if (salespeopleResponse.ok) {
          const salespeopleData = await salespeopleResponse.json();
          let add_num = salespeopleData.salespeople.slice(-1).pop().employee_num + 1

          if (add_num < 1000) {
              this.setState({
                  employeeNum: 1000,
              })
          } else {
          this.setState({
            employeeNum: add_num,
           });
        }
        }
      }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({salesPerson: value})
    }

    handleEmployeeNumChange(event) {
        const value = event.target.value;
        this.setState({employeeNum: value})
    }

    handleReset(event) {
        this.setState(
            {formVisible: "shadow p-4 mt-4",
            successVisible: "d-none"}
        );
    }

    render() {
        return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className={this.state.successVisible}>
                        <div className="alert alert-success mt-4" role="alert">
                            Sales person added successfully!
                        </div>
                        <button className="btn btn-outline-success" onClick={this.handleReset}>
                            Add another!
                        </button>
                    </div>
            <div className={this.state.formVisible}>
            <h1>New Employee Form</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleSalesPersonChange} value={this.state.salesPerson} placeholder="Sales Person" required type="text" name="sales_person" id="sales_person" className="form-control"/>
                <label htmlFor="sales_person">Employee Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEmployeeNumChange} value={this.state.employeeNum} disabled placeholder="Employee Num" required type="number" min="1000" max="1999" name="employee_num" id="employee_num" className="form-control"/>
                <label htmlFor="employee_num">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default AddSalesPerson;
