import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


function ListSales() {

        const [data, setData] = React.useState([]);

        React.useEffect(() => {
            AllowedToVisit()
          const url = `${process.env.REACT_APP_SALES_API}/api/autosales/`;
          fetch(url)
            .then((response) => response.json())
            .then((json) => setData(json['autosales']))
            .catch((error) => console.log(error));
        }, []);

        React.useEffect(() => {
        }, [data]);

    return (
        <div>
            <p></p>
        <h2>Autosale History</h2>
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Employee Number</th>
                    <th>Customer Name</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {data.map(autosale => {
                    return (
                        <tr key={autosale.id}>
                        <td>{ autosale.sales_rep.sales_person }</td>
                        <td>{ autosale.sales_rep.employee_num }</td>
                        <td>{ autosale.customer.customer_name }</td>
                        <td>{ autosale.automobile.vin }</td>
                        <td>$ { new Intl.NumberFormat().format(autosale.price) }</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default ListSales;
