import React, { Component } from "react";
import axiosInstance from "./axiosApi";


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { username: "", password: "", errors: "" };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        try {
            const response = await axiosInstance.post('/token/obtain/', {
                username: this.state.username,
                password: this.state.password
            });
            const data = response.data
            axiosInstance.defaults.headers['Authorization'] = "JWT " + data.access;
            localStorage.setItem('access_token', data.access);
            localStorage.setItem('refresh_token', data.refresh);
            this.setState({
                username: '',
                password: ''
            })
            return window.location.href = '/car-car';
        } catch (error) {
            alert(error.response.data['detail'])
            throw error;
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h2>Login</h2>
                        <form onSubmit={this.handleSubmit}>

                            <div className="form-floating mb-3">
                                <input
                                    className="form-control" onChange={this.handleChange}
                                    required type="text" id="username" placeholder="username"
                                    name="username" value={this.state.username}
                                    minLength={5}
                                    maxLength={15}
                                />
                                <label htmlFor="username">Username</label>
                                <p>{this.state.errors.username ? this.state.errors.username : null}</p>
                            </div>

                            <div className="form-floating mb-3">
                                <input
                                    className="form-control" onChange={this.handleChange}
                                    required type="password" id="password" placeholder="password"
                                    name="password" value={this.state.password}
                                    minLength={8}
                                    maxLength={25}
                                />
                                <p>{this.state.errors.password ? this.state.errors.password : null}</p>
                                <label htmlFor="password">Password</label>
                            </div>

                            <div>
                                <button className="btn btn-primary">Login</button>
                            </div>
                            <div className="mt-2">
                                <p className="forgot-password text-right">
                                    Need to sign up? <a href="/car-car/signup">Sign up</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
    // render() {
    //     return (
    //       <form onSubmit={this.handleSubmit}>
    //         <p></p>
    //         <h3>Sign In</h3>
    //             <div className="mb-3">
    //                 <label>Username</label>
    //                     <input
    //                         name="username"
    //                         type="text"
    //                         value={this.state.username}
    //                         onChange={this.handleChange}
    //                         className="form-control"
    //                         placeholder="Enter username"
    //                         />
    //         </div>
    //             <div className="mb-3">
    //                 <label>Password</label>
    //                     <input
    //                         name="password"
    //                         type="password"
    //                         value={this.state.password}
    //                         onChange={this.handleChange}
    //                         className="form-control"
    //                         placeholder="Enter password"
    //                     />
    //             </div>
    //         <div className="d-grid">
    //           <button type="submit" value="Submit" className="btn btn-dark">
    //                 Submit
    //           </button>
    //         </div>
    // <p className="forgot-password text-right">
    // Need to sign up? <a href="/signup">Sign up</a>
    // </p>
    // {/* <p className="forgot-password text-right">
    //   Forgot <a href="#">password?</a>
    // </p> */}
    //       </form>
    //     )
    //   }

}
export default Login;
