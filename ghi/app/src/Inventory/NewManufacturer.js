import React from 'react';
import AllowedToVisit from "../AllowedToVisit";



class NewManufacturer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            formVisible: "shadow p-4 mt-4",
            successVisible: "d-none",
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
        AllowedToVisit()
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.formVisible;
        delete data.successVisible;

        const manufacturerUrl = `${process.env.REACT_APP_INVENTORY_API}/api/manufacturers/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: '',
                formVisible: "shadow p-4 mt-4 d-none",
                successVisible: "",
            }
            this.setState(cleared);
        } else {
            console.error("invalid request")
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleReset(event) {
        this.setState(
            {formVisible: "shadow p-4 mt-4",
            successVisible: "d-none"}
        );
    }

    render() {
        return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className={this.state.successVisible}>
                        <div className="alert alert-success mt-4" role="alert">
                            Manufacturer added successfully!
                        </div>
                        <button className="btn btn-outline-success" onClick={this.handleReset}>
                            Add another!
                        </button>
                    </div>
            <div className={this.state.formVisible}>
            <h1>New Manufacturer Form</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default NewManufacturer;
