import React from 'react';

class AddServiceAppointment extends React.Component {
    constructor(props) {
        super(props);
        const currentTime = new Date().toISOString();
        this.state={
            vin: "",
            owner: "",
            scheduledTime: currentTime.slice(0, 16),
            technicians: [],
            reason: "",
            formVisible: "shadow p-4 mt-4",
            successVisible: "d-none",
        };
        this.handleReset = this.handleReset.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleOwnerChange = this.handleOwnerChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleScheduledTimeChange = this.handleScheduledTimeChange.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const schedule = new Date(data.scheduledTime);
        data.scheduled_time = schedule.toISOString();
        delete data.scheduledTime;
        delete data.technicians;
        delete data.formVisible;
        delete data.successVisible;

        const url = `${process.env.REACT_APP_SERVICE_API}/api/service/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const cleared = {
                vin: "",
                owner: "",
                scheduledTime: new Date().toISOString().slice(0, 16),
                technician: "",
                reason: "",
                formVisible: "shadow p-4 mt-4 d-none",
                successVisible: "",
                message: `Thanks, ${data.owner}, we look forward to helping you out!`
            };
            this.setState(cleared);
        } else {
            console.error("invalid request")
        }
    }

    handleReset(event) {
        this.setState({
            successVisible: "d-none",
            formVisible: "shadow p-4 mt-4",
        });
        window.location.reload();

    }

    handleScheduledTimeChange(event) {
        const value = event.target.value;
        this.setState({scheduledTime: value});
    }

    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({technician: value});
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value.toUpperCase()});
    }

    handleOwnerChange(event) {
        const value = event.target.value;
        this.setState({owner: value});
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value});
    }

    async componentDidMount() {
        const url = `${process.env.REACT_APP_SERVICE_API}/api/technicians/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({technicians: data.technicians});
        } else {
            console.error("invalid request")
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className={this.state.successVisible}>
                        <div className="alert alert-success mt-4" role="alert">
                            {this.state.message}
                        </div>
                        <button className="btn btn-outline-success" onClick={this.handleReset}>
                            Create another appointment!
                        </button>
                    </div>
                    <div className={this.state.formVisible}>
                            <h1>Create new appointment</h1>
                        <form onSubmit={this.handleSubmit} id="addServiceAppointment">
                            <div className="form-floating mb-3">
                                <input 
                                    className="form-control" onChange={this.handleOwnerChange}
                                    required type="text" id="owner" placeholder="owner"
                                    name="owner" value={this.state.owner}
                                />
                                <label htmlFor="owner">Vehicle owner name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                    className="form-control" onChange={this.handleVinChange}
                                    required type="text" id="vin" placeholder="VIN"
                                    name="vin" value={this.state.vin} maxLength={17}
                                    minLength={17}
                                />
                                <label htmlFor="vin">Vehicle VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                    className="form-control" onChange={this.handleReasonChange}
                                    required type="text" id="reason" placeholder="reason"
                                    name="reason" value={this.state.reason} maxLength={30}
                                />
                                <label htmlFor="reason">Reason for appointment</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input 
                                    className="form-control" onChange={this.handleScheduledTimeChange}
                                    required type="datetime-local" id="scheduledTime" name="scheduledTime"
                                    placeholder="scheduledTime" value={this.state.scheduledTime}
                                />
                                <label htmlFor="scheduledTime">Choose date and time</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select
                                    className="form-select" onChange={this.handleTechnicianChange} 
                                    required id="technician" name="technician" placeholder="technician"
                                    value={this.state.technician}
                                >
                                    <option value="">Choose your technician</option>
                                    {this.state.technicians.map(technician => {
                                        return(
                                            <option key={technician.id} value={technician.id}>
                                                {technician.technician_name}
                                            </option>
                                        );
                                    })}
                                </select>
                                <label htmlFor="technician">Technician</label>
                            </div>
                            <button className="btn btn-primary">Create Appointment</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    };
};

export default AddServiceAppointment;
