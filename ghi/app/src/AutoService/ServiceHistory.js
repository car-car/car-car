import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            appointments: [],
        }
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        AllowedToVisit()
    }

    async handleSubmit(event) {
        event.preventDefault();
        const url = `${process.env.REACT_APP_SERVICE_API}/api/service/history/${this.state.vin}/`;

        const request = await fetch(url);
        if (request.ok) {
            const data = await request.json();
            this.setState({appointments: data.appointments});
        } else {
            console.log("invalid request");
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value.toUpperCase()});
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="offset-1 col-10">
                        <div className="p-4 m-4">
                            <form className="input-group" onSubmit={this.handleSubmit}>
                                <input 
                                    className="form-control" value={this.state.vin} onChange={this.handleVinChange}
                                    id={this.state.vin} name={this.state.vin} maxLength={17}
                                    minLength={17} required type="text" placeholder="VIN"
                                />
                                <button className="btn btn-primary input-group-append">Search VIN</button>
                            </form>
                            <h1 className="mt-3">Service Appointments</h1>
                            <table className="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>VIN</th>
                                        <th>Customer name</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Technician</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.appointments.map(appointment => {
                                        const date = new Date(appointment.scheduled_time);
                                        const mm = date.getMonth() + 1;
                                        const dd = date.getDate();
                                        const yyyy = date.getFullYear();
                                        const dateFormat = yyyy + '-' + mm + '-' + dd;
                                        return (
                                            <tr key={appointment.id}>
                                                <td>{appointment.vin}</td>
                                                <td>{appointment.owner}</td>
                                                <td>{dateFormat}</td>
                                                <td>{date.toLocaleTimeString(
                                                    'en-US',
                                                    {hour: 'numeric', minute:'2-digit'}
                                                    )}
                                                </td>
                                                <td>{appointment.technician.technician_name}</td>
                                                <td>{appointment.reason}</td>
                                            </tr>
                                        )})
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceHistory
