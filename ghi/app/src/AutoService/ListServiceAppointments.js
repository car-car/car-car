import React from 'react';
import AllowedToVisit from "../AllowedToVisit";


class ListServiceAppointments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceAppointments: [],
        }
    };

    async handleClick(id, method) {
        const url = `${process.env.REACT_APP_SERVICE_API}/api/service/${id}/`;

        var fetchConfig;
        if (method === "DELETE") {
            fetchConfig = {
                method: "DELETE",
            }
        } else {
            fetchConfig = {
                method: "PUT",
            }
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.location.reload(false)
        } else {
            console.error(response.status);
        }
    }

    async componentDidMount() {
        AllowedToVisit()
        const url = `${process.env.REACT_APP_SERVICE_API}/api/service/`;

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const appointments = data.appointments.filter( appointment => {
                return (
                    appointment.finished === false
                )
            })
            this.setState({serviceAppointments: appointments});
        } else {
            console.error("invalid request")
        }
    }

    render() {
        return(
            <div>
                <h1 className="mt-4 mb-2">Upcoming Serivce Appointments</h1>
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.serviceAppointments.map(appointment => {
                            const date = new Date(appointment.scheduled_time)
                            return(
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.owner}</td>
                                    <td>{date.toLocaleDateString()}</td>
                                    <td>{date.toLocaleTimeString()}</td>
                                    <td>{appointment.technician.technician_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button 
                                            onClick={() => this.handleClick(appointment.id, "DELETE")}
                                            className="btn btn-danger">
                                            Cancel
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            onClick={() => this.handleClick(appointment.id, "PUT")}
                                            className="btn btn-success">
                                            Finished
                                        </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    };
};

export default ListServiceAppointments;