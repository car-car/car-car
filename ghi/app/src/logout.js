import React from 'react';
import axiosInstance from './axiosApi';

class Logout extends React.Component {
    constructor() {
        super();
        this.handleLogout = this.handleLogout.bind(this);
    }

    async handleLogout() {
        try {
            await axiosInstance.post('blacklist/', {
                "refresh_token": localStorage.getItem("refresh_token")
            });
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            axiosInstance.defaults.headers['Authorization'] = null;
            return window.location.href = '/car-car';
        }
        catch (e) {
            console.log(e);
        }
    };

    render() {
        return (

            <div className="mb-3">
                <p></p>
                <h2>Logout</h2>
                <label>Are you sure you want to log out?</label>
                <p></p>
                <div>
                    <button className="btn btn-danger" onClick={this.handleLogout}>Logout</button>
                </div>
            </div>

        )
    }
}


export default Logout;
