from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.core.validators import (
    MaxValueValidator,
    MinValueValidator,
) 
    

# Create your models here.

class InventoryVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    id_num = models.IntegerField()
    import_href = models.CharField(max_length=35, null=True)

    def __str__(self):
        return f"vin: {self.vin}"

class Technician(models.Model):
    technician_name = models.CharField(max_length=30)
    employee_num = models.IntegerField(
        unique=True,
        validators=[
            MaxValueValidator(2999),
            MinValueValidator(2000),
        ],
    )

    def __str__(self):
        return f"employee # {self.employee_num}: {self.technician_name}"


# vehicle variable is checked against InventoryVO and if there is a
# match, dealer_sold == True and special services are applied.
class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=17)
    owner = models.CharField(max_length=30)
    scheduled_time = models.DateTimeField(default=timezone.now)
    technician = models.ForeignKey(
        "Technician",
        related_name="appointment",
        on_delete=models.PROTECT,
    )
    reason = models.CharField(max_length=40)
    finished = models.BooleanField(default=False)
    dealer_sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.owner}'s vin # {self.vin} checkup for {self.reason}"

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
