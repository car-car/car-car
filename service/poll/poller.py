import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()
INVENTORY_API = os.environ["INVENTORY_API"]

from service_rest.models import InventoryVO

def poll():
    while True:
        print('Service poller polling for data')
        try:
            response = requests.get(f"{INVENTORY_API}/api/automobiles/")
            inventory = json.loads(response.content)
            for vehicle in inventory["autos"]:
                InventoryVO.objects.update_or_create(
                    import_href = vehicle["href"],
                    defaults = {
                        "vin": vehicle["vin"],
                        "id_num": vehicle["id"],
                    },
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
